'use strict';

const { join } = require('path');
const { spawn, fork } = require('child_process');

function isString(value) {
  return typeof value === 'string';
}

function isArray(value) {
  return toString.call(value) === '[object Array]';
}

function isObjectOnly(value) {
  if (toString.call(value) !== '[object Object]') {
    return false;
  }

  let prototype = Object.getPrototypeOf(value);
  return prototype === null || prototype === Object.prototype;
}

function isFunction(value) {
  return toString.call(value) === '[object Function]';
}

function spawning(command, argument, progressOptions, options) {
  return new Promise((resolve, reject) => {
    options = options || {
      stdio: 'pipe',
      sudo: false,
      fork: null,
      onerror: null,
      onprogress: null,
      onmessage: null,
    };

    options.stdio = options.stdio || 'pipe';
    const forked = isString(options.fork) ? fork(options.fork) : null;
    let progress = progressOptions;
    if (isObjectOnly(progressOptions))
      options = Object.assign(options, progressOptions);

    if (isFunction(options.onprogress))
      progress = options.onprogress;

    let error = null;
    let output = null;
    let sudo = options.sudo || false;
    let onerror = options.onerror || null;
    let onmessage = options.onmessage || null;

    delete options.sudo;
    delete options.fork;
    delete options.onerror;
    delete options.onprogress;
    delete options.onmessage;

    if (sudo) {
      argument = [command].concat(argument);
      command = isWindows() ? join(__dirname, 'bin', 'sudo.bat') : 'sudo';
    };

    const spawned = spawn(command, argument, options);
    spawned.on('error', (data) => {

      return reject(data);
    });

    spawned.on('close', (code) => {
      if (code === 0) {
        return resolve(output);
      }

      return reject(error, code);
    });

    spawned.on('exit', (code) => {
      if (forked)
        setTimeout(() => {
          forked.kill();
        }, 1000);

      if (code === 0) {
        return resolve(output);
      }

      return reject(error, code);
    });

    spawned.stdout.on('data', (data) => {
      let input = data.toString();
      output += input;
      try {
        if (isFunction(progress)) {
          output = progress({ spawn: spawned, output: input, fork: forked }) || output;
        }
      } catch (e) {
        return reject(e.toString());
      }

      if (argument.includes('fake-js')) {
        spawned.kill('SIGKILL');
        return resolve('For testing only. ' + output);
      }
    });

    spawned.stderr.on('data', (data) => {
      let err = data.toString();
      error += err;
      if (isFunction(onerror))/* c8 ignore next */
        error = onerror(err) || error;
    });

    if (forked) {
      forked.on('message', (data) => {
        if (isFunction(onmessage))
          onmessage(data);
      });
    }

    if (argument.includes('fake-js') && Object.getOwnPropertyDescriptor(process, 'platform').value == 'darwin') {
      spawned.kill('SIGKILL');
      return resolve('For testing only. ' + output);
    }
  });
}


let archiveTypePattern = /: [A-Z,7]*$/g;

const isInt = function isInt(x) {
  return !isNaN(x) && eval(x).toString().length == parseInt(eval(x)).toString().length;
};

Unar.defaultListFilter = function (s) {
  return s && s != '' &&
    s.indexOf('\r') == -1 &&
    s.indexOf('\n') == -1 &&
    !s.match(archiveTypePattern);
};

/**
 * Unar
 *
 * @param {String} archiveFile
 * @param {String} optionsTarget The directory to write the contents of the archive to. Defaults to the current directory.
 * - Otherwise, `command line options`.
 * @param {String|Array|Object} unpackOptions Only unpack this list of files or directories.
 * - Otherwise, `command line options`.
 * @param {Object} options command line options
 * - targetDir: `String` | The directory to write the contents of the archive to. Defaults to the current directory.
 * - files: `String` | Only unpack this list of files or directories.
 * - forceOverwrite: `true/false` (default) | if null, tmp dir will created automatically
 * - forceDirectory: `true/false/undefined` | Always create a containing directory for the contents of the unpacked archive. By default, a directory is created if there is more than one top-level file or folder.
 * - noDirectory: `true/false/undefined` | Never create a containing directory for the contents of the unpacked archive.
 * - noRecursion: `true/false/undefined` | Do not attempt to extract archives contained in other archives. For instance, when unpacking a .tar.gz file, only unpack the .gz file and not its contents.
 * - copyTime: `true/false/undefined` | Copy the file modification time from the archive file to the containing directory, if one is created.
 * - password: `String` | The password to use for decrypting protected archives.
 * - passwordEncoding: `String` | The encoding to use for the password for the archive, when it is not known. If not specified, then either the encoding given by the -encoding option or the auto-detected encoding is used.
 * - encoding: `String` | The encoding to use for filenames in the archive, when it is not known. If not specified, the program attempts to auto-detect the encoding used. Use "help" or "list" as the argument to give
 *
 * @returns {Promise} Promise
 * @see http://unarchiver.c3.cx/commandline
 */
module.exports.unpack = Unar.unpack = function (archiveFile, optionsTarget, unpackOptions = {}, options) {
  return new Promise((resolve, reject) => {
    options = options || {
      forceOverwrite: true,
      noDirectory: true
    };
    if (isString(optionsTarget)) {
      options.targetDir = optionsTarget;
      if (isString(unpackOptions) || isArray(unpackOptions))
        options.files = unpackOptions;
      else if (isObjectOnly(unpackOptions))
        options = Object.assign(options, unpackOptions);
    } else if (isObjectOnly(optionsTarget)) {
      options = Object.assign(options, optionsTarget);
    }

    options.quiet = false;
    if (!archiveFile)
      archiveFile = options.archiveFile;
    if (!archiveFile)
      return reject("Error: archiveFile or options.archiveFile missing.");

    // Unar command:
    let ar = ['unar'];

    // Archive file (source):
    //ar.push('SOURCEFILE');
    ar.push(archiveFile);

    // -output-directory (-o) <string>: The directory to write the contents of the archive to. Defaults to the current directory.
    ar.push('-o');
    let targetDir = options.targetDir;
    if (!targetDir)
      targetDir = process.cwd();
    ar.push(targetDir);

    // -force-overwrite (-f): Always overwrite files when a file to be unpacked already exists on disk. By default, the program asks the user if possible, otherwise skips the file.
    if (options.forceOverwrite)
      ar.push('-f');

    // -force-rename (-r): Always rename files when a file to be unpacked already exists on disk.
    if (options.forceRename)
      ar.push('-r');

    // -force-skip (-s): Always skip files when a file to be unpacked already exists on disk.
    if (options.forceSkip)
      ar.push('-s');

    // -force-directory (-d): Always create a containing directory for the contents of the unpacked archive. By default, a directory is created if there is more than one top-level file or folder.
    if (options.forceDirectory)
      ar.push('-d');

    // -no-directory (-D): Never create a containing directory for the contents of the unpacked archive.
    if (options.noDirectory)
      ar.push('-D');

    // -no-recursion (-nr): Do not attempt to extract archives contained in other archives. For instance, when unpacking a .tar.gz file, only unpack the .gz file and not its contents.
    if (options.noRecursion)
      ar.push('-nr');

    // -copy-time (-t): Copy the file modification time from the archive file to the containing directory, if one is created.
    if (options.copyTime)
      ar.push('-t');

    // -quiet (-q): Run in quiet mode.
    if (options.quiet)
      ar.push('-q');

    // -password (-p) <string>: The password to use for decrypting protected archives.
    if (options.password) {
      ar.push('-p');
      ar.push(options.password);
    }
    // -password-encoding (-E) <name>: The encoding to use for the password for the archive, when it is not known. If not specified, then either the encoding given by the -encoding option or the auto-detected encoding is used.
    if (options.passwordEncoding) {
      ar.push('-E');
      ar.push(options.passwordEncoding);
    }

    // -encoding (-e) <encoding name>: The encoding to use for filenames in the archive, when it is not known. If not specified, the program attempts to auto-detect the encoding used. Use "help" or "list" as the argument to give
    if (options.encoding) {
      ar.push('-e');
      ar.push(options.encoding);
    }

    if (options.indexes) {
      // -indexes (-i): Instead of specifying the files to unpack as filenames or wildcard patterns, specify them as indexes, as output by lsar.
      if (isArray(options.indexes)) {
        options.indexes.forEach(function (idx) {
          ar.push('-i');
          ar.push('' + idx); // string!
        });
      } else if (isInt(options.indexes)) {
        return reject('options.indexes must be an array of integer, but it is: ' + JSON.stringify(options.indexes))
      }
    } else if (options.files) {
      if (isArray(options.files)) {
        options.files.forEach(function (s) {
          ar.push(s);
        });
      } else {
        ar.push(options.files);
      }
    }

    let run = ar.shift();
    let files = [];
    let type = '';
    let directory = '';
    const onprogress = (data) => {
      let file = null;
      data = data.output;
      if (data.includes('No files extracted') || data.includes('Opening file failed')) {
        return reject('Error: No files extracted');
      } else if (data) {
        data = data.split(/(\r?\n)/g);
        if (!data[0].includes('OK.'))
          type = data[0];
        if (data[0].includes('OK.') || type) {
          data = data[2];
          if (data) {
            if (data.includes('Successfully')) {
              directory = data.split('to "')[1];
              directory = directory.split('"')[0];
            } else {
              file = data.split('  ')[1];
              if (file)
                files.push(file);
            }
          }
        }
        return { type: type, files: files, directory: directory };
      }
    };

    return spawning(run, ar, onprogress)
      .then((result) => {
        return resolve(result);
      })
      .catch((err) => {
        return reject(err);
      });
  }); // unar.unpack
}

/**
 * Lsar
 *
 * @param {String} archiveFile
 * @param {Object} options
 *
 * @returns {Promise} Promise
 * @see http://unarchiver.c3.cx/commandline
 */
module.exports.list = Unar.list = function (archiveFile, options) {
  return new Promise((resolve, reject) => {
    if (!archiveFile)
      archiveFile = options.archiveFile;
    if (!archiveFile)
      return reject("Error: archiveFile or options.archiveFile missing.");

    if (!options)
      options = {};

    // lsar command:
    let ar = ['lsar'];

    // Archive file (source):
    //ar.push('SOURCEFILE');
    ar.push(archiveFile);

    // -no-recursion (-nr): Do not attempt to extract archives contained in other archives. For instance, when unpacking a .tar.gz file, only unpack the .gz file and not its contents.
    if (options.noRecursion)
      ar.push('-nr');

    // -password (-p) <string>: The password to use for decrypting protected archives.
    if (options.password) {
      ar.push('-p');
      ar.push(options.password);
    }
    // -password-encoding (-E) <name>: The encoding to use for the password for the archive, when it is not known. If not specified, then either the encoding given by the -encoding option or the auto-detected encoding is used.
    if (options.passwordEncoding) {
      ar.push('-E');
      ar.push(options.passwordEncoding);
    }

    // -encoding (-e) <encoding name>: The encoding to use for filenames in the archive, when it is not known. If not specified, the program attempts to auto-detect the encoding used. Use "help" or "list" as the argument to give
    if (options.encoding) {
      ar.push('-e');
      ar.push(options.encoding);
    }

    // -print-encoding (-pe): Print the auto-detected encoding and the confidence factor after the file list
    if (options.printEncoding) {
      ar.push('-pe');
      ar.push(options.printEncoding);
    }

    // -json (-j): Print the listing in JSON format.
    if (options.json)
      ar.push('-j');

    // -json-ascii (-ja): Print the listing in JSON format, encoded as pure ASCII text.
    if (options.jsonAscii)
      ar.push('-ja');

    let run = ar.shift();
    return spawning(run, ar)
      .then((result) => {
        let lines = result.split(/(\r?\n)/g);
        if (lines.length > 0) {
          let files = lines.filter(Unar.defaultListFilter);
          if (lines[2]) {
            files.shift();
            return resolve(files);
          }
        }

        throw ('no files found in ' + archiveFile + ' archive.');
      })
      .catch((err) => {
        return reject('Error: ' + err);
      });
  }); // unar.list
}

function Unar() { }
