const download = require('./index.js');
const fs = require('fs');

const testLink = '';

(async function(){
    const dl = await download(testLink).catch(console.error);
    console.log(dl);
    dl.download().then(dlStream=>{
        console.log(dlStream);
        const stream = fs.createWriteStream(dl.filename);
        dlStream.stream.pipe(stream);
    });
})();