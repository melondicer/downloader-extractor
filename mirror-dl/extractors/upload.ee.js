const fetch = require('node-fetch');
const {JSDOM} = require('jsdom');
const {Download, downloadFunc} = require('../util.js');

module.exports = function(url){
    return new Promise(async (resolve, reject)=>{
        const res = await fetch(url.href).catch(reject);
        if(!res) return reject();
        const text = await res.text().catch(reject);
        if(!text) return reject();
        const dom = new JSDOM(text);
        const dlButton = dom.window.document.querySelector('#d_l');
        if(!dlButton) return reject('File does not exist');
        const filename = dom.window.document.querySelector('.pageTitle b')?.textContent;

        const dl = new Download(url, filename, downloadFunc(dlButton.href));
        resolve(dl);
    });
}