const extractors = {
    'anonfiles.com': require('./extractors/anonfilesbayfiles'),
    'anonfile.com': require('./extractors/anonfilesbayfiles'),
    'bayfiles.com': require('./extractors/anonfilesbayfiles'),
    'hexupload.net': require('./extractors/hexupload'),
    'upload.ee': require('./extractors/upload.ee'),
    'send.cm': require('./extractors/send.cm'),
    '1fichier.com': require('./extractors/1fichier')
}

module.exports = function(url){
    const parsedURL = new URL(url);
    parsedURL.protocol = 'https:'; // why people share http links is beyond me

    const extractor = extractors[parsedURL.hostname];
    if(!extractor) return Promise.reject('Unknown file host');

    return extractor(parsedURL);
}