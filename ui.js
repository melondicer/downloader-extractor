const blessed = require('blessed');


class UI{
    constructor(){
        this.screen = new blessed.screen({
            smartCSR: true,
            fullUnicode: true
        });
        this.screen.title = 'Downloader Extractor';

        this.box = blessed.box({
            top: 'center',
            left: 'center',
            width: '80%',
            height: '80%',
            content: '',
            tags: true,
            border: {
              type: 'line'
            }
        });
        this.screen.append(this.box);
        this.title = '';
        this.body = '';
    }

    _updateContent(){
        this.box.setContent(`{bold}{green-fg}${this.title}{/green-fg}{/bold}\n${this.body}`);
        this.screen.render();
    }

    setTitle(text){
        this.title = text;
        this._updateContent();
    }

    error(text){
        this.body = `{red-fg}${text}{/red-fg}`;
        this._updateContent();
    }

    requesting(text){
        this.body = `Requesting {blue-fg}${text}{/blue-fg}`;
        this._updateContent();
    }

    extracting(text){
        this.body = `Extracting {blue-fg}${text}{/blue-fg}`;
        this._updateContent();
    }

    downloading(files){
        this.body = 'Downloading\n' + files.map(file=>{
            return `{blue-fg}${file.filename}{/blue-fg} {yellow-fg}${Math.floor(file.progress / file.filesize * 100)}%{/yellow-fg}`
        }).join('\n');
        this._updateContent();
    }

    end(){
        this.screen.destroy();
    }
}

module.exports = UI;