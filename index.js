const fs = require('fs');
const download = require('mirror-dl');
const {unpack} = require('./unarchiver.js');
const UI = require('./ui.js');

const list = require('./dl.json');

const ui = new UI();

function delay(ms){
    return new Promise(resolve=>{
        setTimeout(resolve, ms);
    });
}

(async()=>{
    await fs.promises.mkdir('extracted', {recursive: true});
    await fs.promises.mkdir('download', {recursive: true});

    for(let file of list){
        ui.setTitle(file.title);
        let dlFiles = [];
        let host;
        for(let link of file.links){
            if(host) if(new URL(link).hostname != host) continue; // skip if there was a successful download for a host and this isn't a second file from said host
            let error;
            ui.requesting(link);
            let fileDL = await download(link).catch(e=>{error=e});
            if(error){
                ui.error(`${link}\n${error}`);
                await delay(2000);
            }
            if(!fileDL) continue;
            host = fileDL.url.hostname;
            dlFiles.push(fileDL);
        }
        if(dlFiles.length == 0){
            ui.error('No working links');
            await delay(4000);
            continue;
        }
        let progress = dlFiles.map(file=>{
            return {filename: file.filename, progress: 0};
        });
        await Promise.allSettled(dlFiles.map(fileDL=>{
            return new Promise(async (resolve, reject)=>{
                let dlStream = await fileDL.download();
                progress.find(file=>file.filename == fileDL.filename).filesize = dlStream.filesize;
                let downloadPath = 'download/' + fileDL.filename;
                let writeStream = fs.createWriteStream(downloadPath);
                dlStream.stream.pipe(writeStream);
                dlStream.stream.on('data', data=>{
                    progress.find(file=>file.filename == fileDL.filename).progress += data.length;
                    ui.downloading(progress);
                });
                dlStream.stream.on('end', resolve);
            });
        }));

        let extractArchive = 'download/' + dlFiles[0].filename;
        ui.extracting('Extracting ' + dlFiles[0].filename);

        let error;
        await unpack(extractArchive, {targetDir: 'extracted/', noDirectory: false, password: file.password}).catch(err=>{error=err});
        if(error){
            ui.error(`${dlFiles[0].filename}\n${error}`);
            delay(4000);
        }
    }
    ui.end();
})();