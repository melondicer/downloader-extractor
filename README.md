# Downloader Extractor
Automatically downloads password protected archives and extracts them.

## Features
- Bulk downloading
- CLI UI
- Allows fallback links from separate file hosts
- Multipart archive support (assumed from multiple links from same file host)

## Installation
Clone the repo and run `npm install`.

## Usage
Create a file called `dl.json` and put downloads in it with the folling structure:
```json
[
    {
        "title": "Multipart example",
        "links": [
            "https://example.com/download.part1",
            "https://example.com/download.part2"
        ],
        "password": "hunter1"
    },
    {
        "title": "Fallback example",
        "links": [
            "https://host1.com/download",
            "https://host2.com/download"
        ],
        "password": "monkey"
    }
]
```
And run `npm start`
