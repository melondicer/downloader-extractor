const fetch = require('node-fetch');
const {JSDOM} = require('jsdom');
const {Download, downloadFunc} = require('../util.js');
const fs = require('fs');

module.exports = function(url){
    url.protocol = 'https:';
    return new Promise(async (resolve, reject)=>{
        const res = await fetch(url.href, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Referer': url.href
            },
            body: `op=download2&id=${url.pathname.slice(1)}&rand=&referer=${encodeURIComponent(url.href)}&method_free=Free+Download&method_premium=&adblock_detected=0`,
            method: 'POST'
        });
        if(!res) return reject();
        const text = await res.text().catch(reject);
        if(!text) return reject();
        const dom = new JSDOM(text);
        const dlButton = dom.window.document.querySelector('.btn, .btn-lg, .btn-success');
        if(!dlButton) return reject('File does not exist');
        const filename = dom.window.document.querySelector('nobr b')?.textContent;

        const dl = new Download(url, filename, downloadFunc(dlButton.href));
        resolve(dl);
    });
}