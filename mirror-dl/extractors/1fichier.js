const fetch = require('node-fetch');
const {JSDOM} = require('jsdom');
const {Download, downloadFunc} = require('../util.js');

module.exports = function(url){
    return new Promise(async (resolve, reject)=>{
        url.protocol = 'https:';
        const res = await fetch(url.href).catch(reject);
        if(!res) return reject();
        if(!res.ok) return reject();
        const text = await res.text().catch(reject);
        if(!text) return reject();
        const dom = new JSDOM(text);
        const adz = dom.window.document.querySelector('input[name=adz]')?.value;
        const filename = dom.window.document.querySelector('tr td[class="normal"]')?.textContent;
        if(!adz || !filename) return reject('File does not exist');

        const postRes = await fetch(url.href, {
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
            referrer: url.href,
            body: 'adz=' + adz,
            method: 'POST',
        }).catch(reject);
        if(!postRes) return reject();
        if(!postRes.ok) return reject();
        const postText = await postRes.text().catch(reject);
        if(!postText) return reject();
        
        const postDom = new JSDOM(postText);
        const dlButton = postDom.window.document.querySelector('a[class="ok btn-general btn-orange"]');
        if(!dlButton) return reject('File does not exist');

        const dl = new Download(url, filename, downloadFunc(dlButton.href));
        resolve(dl);
    });
}