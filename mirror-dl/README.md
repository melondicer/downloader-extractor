# mirror-dl

Library for downloading files from a wide range of file hosts.

# Support
Supported | Host
---|---
✅ | anonfiles.com
✅ | bayfiles.com
✅ | hexupload.net
✅ | upload.ee
✅ | send.cm
✅ | 1fichier.com
❌ | solidfiles.com
❌ | tusfiles.com
❌ | dropbox.com
❌ | mega.io
❌ | mediafire.com
❌ | box.com
❌ | drive.google.com
❌ | disk.yandex.com

# API

The library consists of one function to request a download page, that returns some base classes.

**download(url)**
- `url` Link to the download page.
- Returns: `Promise`<`Download`>

### **Download** class

*Properties*
- `filename`: `String`

*Methods*
- `download`: Returns `Promise`<`DownloadStream`>

### **DownloadStream** class

*Properties*
- `stream`: `stream.Readable`
- `filesize`: `Number`
