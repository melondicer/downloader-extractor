const fetch = require('node-fetch');

class Download{
    constructor(url, filename, download){
        Object.assign(this, {url, filename, download});
    }
}

class DownloadStream{
    constructor(stream, filesize){
        Object.assign(this, {stream, filesize});
    }
}

function downloadFunc(url, headers){
    return (function(){
        return new Promise(async (resolve, reject)=>{
            const res = await fetch(url, {headers}).catch(reject);
            if(!res) return reject();
            if(!res.ok) return reject();

            const length = res.headers.get('Content-Length');
            resolve(new DownloadStream(res.body, Number(length)));
        });
    });
}

function downloadRes(res){
    return (function(){
        const length = res.headers.get('Content-Length');
        return new DownloadStream(res.body, Number(length));
    });
}

module.exports = {
    Download,
    downloadFunc,
    downloadRes
}