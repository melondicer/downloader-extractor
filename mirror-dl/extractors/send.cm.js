const fetch = require('node-fetch');
const {JSDOM} = require('jsdom');
const {Download, downloadRes} = require('../util.js');

module.exports = function(url){
    return new Promise(async (resolve, reject)=>{
        let id;
        if(url.pathname.startsWith('/d/')){
            const tempRes = await fetch(url.href);
            if(!tempRes) return reject();
            if(!tempRes.ok) return reject();
            const html = await tempRes.text();
            if(!html) return tempRes.reject();
            const dom = new JSDOM(html);
            id = dom.window.document.title;
        }else{
            id = url.pathname.slice(1);
        }

        const res = await fetch('https://send.cm/', {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            referrer: "https://send.cm/d/EF4u",
            body: `op=download2&id=${id}&rand=&referer=&method_free=&method_premium=`,
            method: 'POST'
        }).catch(reject);
        if(!res) return reject();
        if(!res.ok) return reject();

        const encodedFilename = res.url.split('/').pop();
        const filename = decodeURIComponent(encodedFilename);

        const dl = new Download(url, filename, downloadRes(res));
        resolve(dl);
    });
}